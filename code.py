# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 18:19:01 2017

@author: annea

import pandas as pd
from sklearn import linear_model
import matplotlib.pyplot as plt
import numpy as np

data = pd.read_csv('challenge_dataset.txt', header = None)
data.columns = ["a" , "b"]

data.drop(data.index[[11,67,80]],axis=0 , inplace=True)
x_values=data[["a"]]
y_values=data[["b"]]
x_values_test=[14.1640 , 10.2360 , 5.7292] #x values at index 11,67,80
y_values_test=[15.50500 , 7.77540 , 0.47953] #yvalues at index 11,67,80
#dataframe.drop(x_values.index[[10]])
#print(data)
#print(x_values)
print(y_values_test)

#train model on data 


reg=linear_model.LinearRegression()
def predicts(x_values,y_values,x):
   
    
    #reg=linear_model.LinearRegression()
    reg.fit(x_values,y_values)

    plt.scatter(x_values,y_values)
    plt.plot(x_values,reg.predict(x_values))
    plt.xlabel("x_values")
    plt.ylabel("y_values")
    plt.title("linear regression")
    plt.legend()
    plt.show()


    return float(reg.predict(x))
predicted=[]
for i in range(0,3):
    
    predicted.append(predicts(x_values,y_values, x_values_test[i] ))
    #print(type(predicted))#list
    #print(type(x_values_test[i]) #float
    print(predicted)
    #print(predicted[i])
    


#actualvalues #testing data
#14.1640  15.50500 index 11
#10.2360   7.77540 index 67
#5.7292   0.47953 index 80

#predicted values
#14.1640   12.92109836 index 11
#10.2360   8.2909201 index 67
#5.7292   2.97847423 index 80

#error calculation example from scikit learn
# The mean squared error
#print("Mean squared error: %.2f" % np.mean((reg.predict(x_values) - y_values) ** 2))


print("Mean squared error: %.2f" % np.mean((np.array(predicted) - np.array(y_values_test)) ** 2))       
for i in range(0,3): 
    y = np.array(y_values_test[i])  
    y1 = np.array((predicted[i]))     
   #print("accuracy:" (np.array(y_values_test[i])-np.array((predicted[i])) / np.array(y_values_test[i])*100))
    print(y)
    print(y1)
    accuracy=(((y1)/y) * 100)
    print("accuracy is ", accuracy, "%")
    